#!/usr/bin/python
# -*- coding: utf-8 -*-

# Librerias
from flask import Flask
from flask import render_template
from flask import jsonify
from flask import request
import socket
import serial
from gpiocontrol import GPIOControl
from subprocess import call
import time
import sqlite3 as lite
from collections import OrderedDict
import time
from datetime import datetime
import shutil
import re

# ------------------------------------------------

FILE_LOG = "/home/pi/elea/alertsdesktop/telegrams.log"
FILE_DB = '/home/pi/elea/alertsdesktop/data.db'
FILE_WPA = 'wpa_supplicant.conf'
DIR_WPA = "/etc/wpa_supplicant/"

# ------------------------------------------------

app = Flask(__name__)

EO_GPIO = GPIOControl()

# ------------------------------------------------

def getNetwork():
	try:
	      s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	      s.connect(("8.8.8.8", 80))
	      localip=s.getsockname()[0]
	      s.close()
	except:
	      localip="Sin red"
	return localip

# ------------------------------------------------

def loadDevices():
	with lite.connect(FILE_DB) as con:
		cur=con.cursor()
		rows = cur.execute("SELECT * FROM devices")
		out=[]
		
		for dev in rows:
			out.append(dev)

	return out

def loadDesktop():
	with lite.connect(FILE_DB) as con:
		cur=con.cursor()
		rows = cur.execute("SELECT * FROM desktop").fetchall()
		out=[]
		
		for row in rows:
			if row[6]:
				diff = round((datetime.now()-datetime.strptime(row[6],"%Y-%m-%d %H:%M:%S.%f")).total_seconds())
			else:
				diff = 0
			
			css = "none"
			if row[5]=="BateriaBaja": css = "warning" 
			if row[4]=="Alerta" : css = "danger" 

			dev = dict(
				name = row[1],
				description = row[2],
				tag = row[3],
				value = row[4],
				battery = row[5],
				time = row[6],
				css = css,
				since = str(diff),
				type = "device",
				info = None
			)

			out.append(dev)

		rows = cur.execute("SELECT * FROM gateways").fetchall()

		for row in rows:

			if row[5]:
				diff = round((datetime.now()-datetime.strptime(row[5],"%Y-%m-%d %H:%M:%S.%f")).total_seconds())
			else:
				diff = 0

			css = "none"
			if row[4]=="BateriaBaja": css = "warning" 
			if row[4]=="Alerta" : css = "danger" 

			gw = dict(
				name = row[1],
				description = row[2],
				tag = row[3],
				type = "gateway",
				value = row[4] if row[4] is not None else "Reset",
				time = diff,
				battery = "BateriaBaja" if row[4]=="BateriaBaja" else "ok",
				css = css,
				info = row[6]
			)

			out.append(gw)

	return out

# ------------------------------------------------

@app.route("/devices")
def devices():
	return jsonify(dict(devices=loadDevices(),desktop=loadDesktop()))

# ------------------------------------------------

@app.route("/devices/<string:name>", methods=["GET","POST"] )
def device(name):

	with lite.connect(FILE_DB) as con:
		cur=con.cursor()

		dev=dict(
			name=			"",
			description=	"",
			tag=			""
		)

		row=cur.execute("SELECT * FROM desktop WHERE name=:id",{"id":name}).fetchone()
		if row:
			dev['name'] 		= row[1]
			dev['description'] 	= row[2]
			dev['tag'] 			= row[3]
			dev['type']			= "device"
		else:
			row=cur.execute("SELECT * FROM gateways WHERE name=:id",{"id":name}).fetchone()
			if row:
				dev['name'] 		= row[1]
				dev['description'] 	= row[2]
				dev['tag'] 			= row[3]
				dev['type']			= "gateway"

		if request.method == 'POST':
			req=request.get_json(force=True)

			if req['type']=='device':
				table='desktop'
			else:
				table='gateways'

			if row:
				try:
					cur.execute("UPDATE "+table+" SET name=?,description=?,tag=? WHERE name = ?",(req['name'], req['description'], req['tag'], name))
					con.commit();
					return jsonify( dict(status="ok"))
				except:
					return jsonify( dict(status="error actualizando"))
			else:
				try:
					cur.execute("INSERT INTO "+table+"(name,description,tag) VALUES (?,?,?)",(req['name'], req['description'], req['tag']))
					con.commit();
					return jsonify( dict(status="ok nuevo"))
				except:
					return jsonify( dict(status="error creando"))

		return jsonify(dev)

# ------------------------------------------------

@app.route("/devices/<string:name>/delete", methods=["POST"] )
def deviceDelete(name):

	with lite.connect(FILE_DB) as con:
		cur=con.cursor()
		try:
			row=cur.execute("SELECT * FROM desktop WHERE name=:id",{"id":name}).fetchone()
			row2=cur.execute("SELECT * FROM gateways WHERE name=:id",{"id":name}).fetchone()

			if row:
				cur.execute("DELETE FROM desktop WHERE name=:id",{"id":name})
				con.commit()
				cur.close()
				return jsonify( dict(status="ok"))
			else:
				if row2:
					cur.execute("DELETE FROM gateways WHERE name=:id",{"id":name})
					con.commit()
					cur.close()
					return jsonify( dict(status="ok"))
			return jsonify( dict(status="Nada que borrar") )
		except:
			cur.close()
			return jsonify( dict(status="Error borrando") )

# -------------------------------------------------

# -------------------------------------------------

@app.route("/devices/<string:name>/reset")
def devicereset(name):
    
	with lite.connect(FILE_DB) as con:
		cur=con.cursor()
		row=cur.execute("SELECT * FROM desktop WHERE name=:id",{"id":name}).fetchone()
		row2=cur.execute("SELECT * FROM gateways WHERE name=:id",{"id":name}).fetchone()

		tabla = "desktop"
		if row2: tabla = "gateways"

		try:
			cur.execute("UPDATE "+tabla+" SET value=? WHERE name = ?",("Reset", name))
			con.commit();
			return jsonify( dict(status="ok"))
		except:

			pass

	return jsonify( dict(status="error"))

# -------------------------------------------------

@app.route("/last_telegram")
def last_telegram():

	with lite.connect(FILE_DB) as con:
		cur=con.cursor()

		try:
			row=cur.execute("SELECT * FROM devices ORDER BY time DESC").fetchone()
			diff = (datetime.now()-datetime.strptime(row[5],"%Y-%m-%d %H:%M:%S.%f")).total_seconds()
			return jsonify(dict( id = diff, name = row[1]))
		except:
			return jsonify(dict( id = 999999, name = "-"))

# -------------------------------------------------

@app.route("/beep/<int:num>")
def beep(num):
	for i in range(num):
		EO_GPIO.Beep(0.5)
		time.sleep(0.5)
		EO_GPIO.Off()
	return jsonify( dict(status="ok" ) )

#--------------------------------------------------

@app.route("/beep")
def beepone():
	EO_GPIO.Beep(1)
	EO_GPIO.Off()
	time.sleep(1)
	return jsonify( dict(status="ok" ) )

#--------------------------------------------------

@app.route("/telegram_log")
def telegram_log():
	try:
		with open(FILE_LOG,"r") as f:
			content = f.readlines()
			f.close()
			return jsonify( dict(log="".join(content)))
	except:
		return jsonify( dict(status="File Exception"))

#--------------------------------------------------

@app.route("/poweroff")
def poweroff():
	call("sudo shutdown now", shell=True)
	return

@app.route("/reset")
def reset():
	call("sudo shutdown -r now", shell=True)
	return

#--------------------------------------------------

@app.route("/configure", methods = ["GET","POST"])
def configure():

	if request.method == 'GET':
		with open(DIR_WPA+FILE_WPA,"r") as f:
			data=f.read()
			f.close()
		ssid = re.search('ssid="(.+?)"',data).group(1)
		password = re.search('psk="(.+?)"',data).group(1)
		return jsonify( dict( result="ok", ssid=ssid, password=password))

	if request.method == 'POST':
		out = render_template(FILE_WPA+".template",ssid=request.form.get('ssid'), password=request.form.get('password'))
		with open(FILE_WPA,"w") as f:
			f.write(out)
			f.close()
		try:
			shutil.copy(FILE_WPA,DIR_WPA+FILE_WPA)
		except:
			return jsonify( dict(status = "error copiando wpa_supplicant") )
		return out


#--------------------------------------------------

@app.route('/')
def hello():
	devices = loadDesktop()
	out = {}

	for dd in devices:

		diff = 0
		css="none"

		if dd['type']=='device':
			if dd["time"]:
				diff = round((datetime.now()-datetime.strptime(dd["time"],"%Y-%m-%d %H:%M:%S.%f")).total_seconds())
			if dd["value"] == "Alerta":
				css = "danger"
			if dd["battery"] == "BateriaBaja":
				css = "warning"

		dd['css'] = css
		dd['time'] = diff

		if dd["tag"] not in out:
			out[dd["tag"]] = [dd]
		else:
			out[dd["tag"]].append(dd)
	outt = OrderedDict(sorted(out.items()))

	return render_template("home.html",devices=outt,localip=getNetwork());

#--------------------------------------------------

if __name__ == '__main__':
    app.run(host= '0.0.0.0')
