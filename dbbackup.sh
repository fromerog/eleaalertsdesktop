#!/bin/sh
# This will dump all your databases

DATE=$(date +%Y%m%d%H%M)

mysqldump --opt --user=EleaAlertsDesktop --password=3l341234 EADesktop > /home/pi/eleaalertsdesktop/web/backup/EADesktop_${DATE}.sql
mysqldump --opt --user=EleaAlertsDesktop --password=3l341234 EADesktop > /home/pi/eleaalertsdesktop/web/EADesktop.sql


# purge old dumps
find /home/pi/eleaalertsdesktop/web/backup/ -name "*.sql*" -mtime +8 -exec rm -vf {} \;
