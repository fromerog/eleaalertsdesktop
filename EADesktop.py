#!/usr/bin/python
# -*- coding: utf-8 -*-

# Librerias
import serial
import RPi.GPIO as GPIO
import sqlalchemy as db
import keyboard
import time
from datetime import datetime


# Configuracion del GPIO para habilitar el modulo EnOcean
GPIO.setmode(GPIO.BOARD)
GPIO.setup(33,GPIO.OUT)
GPIO.output(33,False)

# Configuracion del puerto serie
puerto = serial.Serial('/dev/ttyAMA0',57600, timeout=1)

# Captura de datos ESP3 por puerto serie

# Tipos de telegramas ESP3. Campo RORG
telegram_type = {
    'F6' : 'RPS',
    'D5' : '1BS'
}

# Tipos de datos

telegram_data = {
    'AF' : { "name" : "Alerta", "level" : 10, "text":"Alarma de pulsador" },
    'F2' : { "name" : "Reset", "level" : 0, "text":"Pulsador reseteado" },
    'F1' : { "name" : "BateriaBaja", "level" : 5, "text":"Bateria baja" },
    'F0' : { "name" : "BateriaOk", "level" : 0, "text":"Bateria ok" },
    'FC' : { "name" : "Mantenimiento", "level" : 5, "text":"Revisión de mantenimiento" }
}





engine = None
connection = None
history = None
devices = None
#alarms = None

# Cadena de conexión con BBDD
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://EleaAlertsDesktop:3l341234@localhost:3306/EADesktop'



def getTelegram():
    global telegram_type
    global puerto
    
    try:
       puerto.flushInput()
       lectura = puerto.read(21)
       if lectura :
           word = lectura.encode("hex").upper()
           cadena = ([word[i:i+2] for i in range(0, len(word), 2)])
           print "["+datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "] "+''.join(cadena)
           
           if cadena[6].upper() not in telegram_type:
                return None
           
           telegram = {
            'RORG'  : telegram_type[cadena[6].upper()],
            'data'  : cadena[7].upper(),
            'device': (cadena[8]+cadena[9]+cadena[10]+cadena[11]).upper(),
            'RSSI'  : (int(cadena[18],16)-pow(2,7))
            }
           
           return telegram
       else:
           return None       
    except serial.serialutil.SerialException:
        print "Serial Exception -------------"
        pass
    
    
def storeTelegram(tel): 
    global connection
    global devices
    global history
    global telegram_data
    
    if tel:
        query = devices.select(devices.c.id).where(devices.c.name == tel['device'])
        result = connection.execute(query)
        
        try:
            tel_id = result.fetchone()[0]
      
            if tel_id:
               
                event = telegram_data.setdefault(tel['data'], { 'name' : "Unknown", 'level' : 0, 'text':'Nothing' })
                
                if event['name']=="BateriaBaja":
                    print "BateriaBaja"
                    query = devices.update().where(devices.c.id==tel_id).values( battery = "low", RSSI = tel['RSSI'])
                elif event['name']=="BateriaOk":
                    print "BateriaOk"
                    query = devices.update().where(devices.c.id==tel_id).values( battery = "ok", RSSI = tel['RSSI'])
                else:
                    query = devices.update().where(devices.c.id==tel_id).values( status = event['name'], RSSI = tel['RSSI'])
                
                connection.execute(query);
                
                query = history.insert().values(RORG=tel['RORG'], data=tel['data'], device=tel['device'], RSSI=tel['RSSI']) 
                connection.execute(query)
                
                # processAlarm(event,tel_id)
                
        except:
            query = history.insert().values(RORG=tel['RORG'], data=tel['data'], device=tel['device'], RSSI=tel['RSSI'])
            connection.execute(query)
            print 'New Device Found ('+tel['device']+')'
            return None
        
    else:
        return None
    

def processAlarm(event, tel_id):
    global connection
    global devices
    global history
    #global alarms
    
    query = devices.select().where(devices.c.id == tel_id)
    dev = connection.execute(query).fetchone()
    
    print event
    print dev


    try:
        query = alarms.select().where( and_(alarms.c.devices_id == tel_id, alarms.c.status=='active') )
        alarm= connection.execute(query).fetchone()
        
    except Exception:
        alarm=None
        pass
    
    print alarm

    try:
        #code
        if event['name']=='Alarma':
            print '*** '
            if alarm is None:
                print '*** '
                query = alarms.insert().values(device_id=tel_id, type=event['name'], info=event['name'], status='active', level=event['level']) 
                connection.execute(query)  
        
        if envent['name']=='Reset':
            print "--- "
            if alarm is not None:
                print "--- "
                query = alarms.update(alarm.c.id==alarm['id']).values(status='solved') 
                connection.execute(query)

    except Exception:
        print 'Error Alarma'
        pass
        
    

def initBBDD():
        global engine
        global connection
        global history
        global devices
        global alarms
    
        engine = db.create_engine(SQLALCHEMY_DATABASE_URI, echo=False)
        print 'DB Tables : '
        print(engine.table_names())
        connection = engine.connect()
        metadata = db.MetaData()
        history = db.Table('history', metadata, autoload=True, autoload_with=engine)
        devices = db.Table('devices', metadata, autoload=True, autoload_with=engine)
        # alarms = db.Table('alarms', metadata, autoload=True, autoload_with=engine)

        print "History structure:"
        print(history.columns.keys())
        print "Devices structure:"
        print(devices.columns.keys())
        print "Alarms structure:"
        print(alarms.columns.keys())    
        return connection
    
def main():
    print "Init Database"
    print "============="
    try:
        connection = initBBDD()
    except:
        print "Problema conectando a la base de datos"

    print "Listening to EleaAlerts Network - '^C' to exit"
    print "=============================================="
    
    exiting = False;
    
    while not exiting:
        try:
            tel = getTelegram()
            if tel:
                storeTelegram(tel)
        except (KeyboardInterrupt, SystemExit):
            exiting = True
            break
    
if __name__ == '__main__':
    main()
