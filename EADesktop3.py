#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Librerias
import RPi.GPIO as GPIO
import keyboard
import time
from datetime import datetime
from enocean.consolelogger import init_logging
import enocean.utils as utils
from enocean.communicators.serialcommunicator import SerialCommunicator
from enocean.protocol.packet import RadioPacket
from enocean.protocol.constants import PACKET, RORG
import sys
import traceback
import logging
import sqlite3 as lite

try:
    import queue
except ImportError:
    import Queue as queue

# Constantes

BUZZER_GPIO = 18
#BUZZER_GPIO = 40  # para el caso de que usemos la placa enocean
ENOCEAN_GPIO = 33
FILE_LOG = "/home/pi/elea/alertsdesktop/telegrams.log"
FILE_DB = '/home/pi/elea/alertsdesktop/data.db'

beep = True     # emite sonido con cada telegrama recibido
#beep = False   # no emite sonido con cada telegrama recibido
sendReset = True # envia el reset al recibir una alarma
#sendReset = False # no envia el reset


# Configuracion del GPIO para habilitar el modulo EnOcean
GPIO.setmode(GPIO.BOARD)
GPIO.setup(ENOCEAN_GPIO,GPIO.OUT)
GPIO.output(ENOCEAN_GPIO,False)

GPIO.setmode(GPIO.BOARD)
GPIO.setup(BUZZER_GPIO, GPIO.OUT)

if beep:
    GPIO.output(BUZZER_GPIO, True)
    time.sleep(0.5)
    GPIO.output(BUZZER_GPIO, False)


# Configure logger

logging.basicConfig(filename='/home/pi/elea/alertsdesktop/AlertsDesktop.log', level=logging.DEBUG,
                    format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %H:%M:%S ')
logging.getLogger('enocean').propagate = True


def main():
    
    global EnoTelegrama
    
    init_logging()
    communicator = SerialCommunicator()
    communicator.start()
    time.sleep(1)
    EnoTelegrama = RadioPacket.create(RORG.RPS, 0x02, 0x06, None)
    EnoTelegramaVLD = RadioPacket.create(RORG.VLD, 0x01, 0x01, None)

    open(FILE_LOG, 'w').close()

    print("\nListening to EleaAlerts Network - '^C' to exit\n==============================================")

    exiting = False;

    while communicator.is_alive() and (exiting==False):
        try:
            packet = communicator.receive.get(block=True, timeout=1)
                  
            if (packet.packet_type == PACKET.RADIO_ERP1) and ((packet.rorg == RORG.RPS) or (packet.rorg == RORG.VLD)):
                
                ID = utils.to_hex_string(packet.data[2]) + utils.to_hex_string(packet.data[3]) + utils.to_hex_string(packet.data[4]) + utils.to_hex_string(packet.data[5])
                dato  = utils.to_hex_string(packet.data[1])
                status = utils.to_hex_string(packet.data[6])
                
                tel={
                    'RORG'  : 'RPS',
                    'data'  : dato,
                    'device': ID,
                    'RSSI'  : str(packet.dBm),
                    'status' : status
                }
                
                tel['RORG'] = 'VLD' if packet.rorg == RORG.VLD else 'RPS'
                
                f = open(FILE_LOG,"a+")
                f.write("["+datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "]  "+' '.join(tel.values())+"\n")
                f.close()

                with lite.connect(FILE_DB) as con:
                    cur=con.cursor()
                    cur.execute("SELECT id FROM devices WHERE name=:id",{"id":ID})
                    row = cur.fetchone();
                    if row is None:
                        # new device found
                        cur.execute("INSERT INTO devices(name,status,time,rssi,data) VALUES(?,?,?,?,?);",(ID,status,datetime.now(),packet.dBm,dato))
                        con.commit()
                    else:
                        cur.execute("UPDATE devices SET name=?,status=?,time=?,rssi=?,data=? WHERE id=?",(ID,status,datetime.now(),packet.dBm,dato,row[0]))
                        con.commit()

                    # update desktop if proceed
                    entry = cur.execute("SELECT * FROM desktop WHERE name=:id",{"id":ID})
                    if entry:
                        value=None
                        battery = "ok"
                        if dato == "AF":
                            value= "Alerta"
                            if sendReset:
                                EnoTelegrama.data[1] = 0xF3    
                                EnoTelegrama.data[6] = 0x20
                                communicator.send(EnoTelegrama)
                                print("\t\t-- " + "Sending F3 (RPS)")
                        elif dato == "20":  #detector de caidas
                            value= "Alerta"
                        elif dato == "F2":
                            value= "Reset"
                        elif dato == "F0":
                            battery= "ok"
                        elif dato == "F1":
                            battery= "BateriaBaja"

                        cur.execute("UPDATE desktop SET value=?, battery=?, time=? WHERE name = ?",(value, battery, datetime.now(), ID))
                        con.commit(); 
                       
                print("["+datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "]  "+'-'.join(tel.values()))
                    
        except IndexError:
            print ("IndexError")
            continue

        except queue.Empty:
            continue

        except (KeyboardInterrupt, SystemExit):
            exiting = True
            break
    
        except Exception:
            traceback.print_exc(file=sys.stdout)
            exiting = True
            break    

    if communicator.is_alive():
       communicator.stop()

    
if __name__ == '__main__':
    main()
