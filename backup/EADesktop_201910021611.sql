-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnueabihf (armv7l)
--
-- Host: localhost    Database: EADesktop
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alarms`
--

DROP TABLE IF EXISTS `alarms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `info` text NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alarms`
--

LOCK TABLES `alarms` WRITE;
/*!40000 ALTER TABLE `alarms` DISABLE KEYS */;
/*!40000 ALTER TABLE `alarms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `info` varchar(128) NOT NULL,
  `location` varchar(128) NOT NULL,
  `status` varchar(16) NOT NULL,
  `created_in` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_time_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `RSSI` int(11) NOT NULL,
  `battery` varchar(16) NOT NULL DEFAULT 'ok',
  `tag` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (16,'058AB54B','Baño minusvalidos','Baño','Reset','0000-00-00 00:00:00','2019-08-09 15:51:21',1,-68,'ok','Planta 6'),(20,'058AB510','Baño minusvalidos','Baño','Reset','0000-00-00 00:00:00','2019-08-09 19:16:54',1,-71,'ok','Planta 5'),(21,'058AB524','Baño minusvalidos','Baño','','0000-00-00 00:00:00','2019-08-09 16:05:44',1,-68,'ok','Planta 4'),(23,'058AB520','Baño minusvalidos','Baño','Reset','0000-00-00 00:00:00','2019-08-09 08:45:33',1,-71,'ok','Planta 3'),(26,'058AB52C','Baño minusvalidos','Baño','','0000-00-00 00:00:00','2019-08-09 16:11:29',1,-67,'ok','Planta 2'),(27,'058AB52E','Baño minusvalidos','Baño','','0000-00-00 00:00:00','2019-08-09 20:01:06',1,-71,'ok','Planta 1'),(30,'050ECCD0','EleaAlertsDesktop','TEST','Reset','0000-00-00 00:00:00','2019-08-05 14:16:03',1,-73,'ok','TEST');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `RORG` varchar(8) DEFAULT NULL,
  `device` varchar(16) NOT NULL,
  `data` varchar(2) DEFAULT NULL,
  `RSSI` int(11) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`device`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` VALUES (0,'RPS','000896CC','40',-89,'2019-08-05 16:19:28','2019-08-05 16:19:28'),(0,'RPS','019D7592','F2',-61,'2019-09-19 08:17:55','2019-09-19 08:17:55'),(0,'RPS','0506AB96','AF',-56,'2019-10-02 11:48:40','2019-10-02 11:48:40'),(0,'RPS','050C97C2','40',-74,'2019-10-02 14:09:52','2019-10-02 14:09:52'),(0,'RPS','050CB081','80',-45,'2019-09-03 08:26:45','2019-09-03 08:26:45'),(0,'RPS','051221A2','F0',-58,'2019-10-02 11:26:15','2019-10-02 11:26:15'),(0,'RPS','0582F492','AF',-73,'2019-08-05 14:18:53','2019-08-05 14:18:53'),(0,'RPS','0582F4BD','AF',-79,'2019-08-05 14:29:41','2019-08-05 14:29:41'),(0,'RPS','058A61F1','F3',-76,'2019-08-05 14:31:29','2019-08-05 14:31:29'),(0,'RPS','058AB4EC','F3',-74,'2019-08-05 14:32:27','2019-08-05 14:32:27'),(0,'RPS','058AB4FC','F3',-76,'2019-08-05 14:36:41','2019-08-05 14:36:41'),(0,'RPS','058AB51D','F3',-82,'2019-08-05 14:27:50','2019-08-05 14:27:50'),(0,'RPS','058AB52A','F0',-70,'2019-08-09 09:15:36','2019-08-09 09:15:36'),(0,'RPS','058AB530','F0',-45,'2019-09-05 14:44:45','2019-09-05 14:44:45'),(0,'RPS','058AB533','F0',-71,'2019-08-09 08:22:11','2019-08-09 08:22:11'),(0,'RPS','058AB538','F2',-77,'2019-08-05 14:36:47','2019-08-05 14:36:47'),(0,'RPS','058AB53D','F0',-68,'2019-08-09 17:22:55','2019-08-09 17:22:55'),(0,'RPS','058AB53F','F0',-79,'2019-08-05 14:33:39','2019-08-05 14:33:39'),(0,'RPS','058AB546','F0',-73,'2019-08-09 15:34:45','2019-08-09 15:34:45'),(0,'RPS','058AB54C','F0',-71,'2019-08-09 19:21:29','2019-08-09 19:21:29'),(0,'RPS','058AB553','F0',-68,'2019-08-09 11:13:25','2019-08-09 11:13:25'),(0,'RPS','FEFF0613','00',-64,'2019-09-03 07:55:33','2019-09-03 07:55:33');
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-02 16:11:14
