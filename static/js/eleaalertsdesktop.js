var activate_sound;


function checkAlarm(){
    
    
    console.log("checkAlarm()");

    //console.log(activate_sound);

    
    jQuery.getJSON( "/devices", function( data ) {
        var items = [];
        
        //console.log(data);
        
        $.each( data.desktop, function() {

            console.log(this);
            
            var isactive = false;

            var name = this.name;
            var status = this.value;
            var css = this.css;
            var bateria = this.battery;

            if(this.type=="gateway") $("#alarm-"+name+" .description").text(this.info);                

            if( $("#alarm-"+name).hasClass("bg-warning") || $("#alarm-"+name).hasClass("bg-danger") ) isactive=true; 
                        
            $("#list-"+name).removeClass("bg-warning bg-danger").addClass("bg-"+css);
            if(css=="warning") css="none";
            $("#alarm-"+name).removeClass("bg-warning bg-danger bg-none").addClass("bg-"+css);


            if ( (status=="Alerta") && (!isactive) ) {
                $.get( "/beep/3" );
                activate_sound=true;
                $(".snooze").removeAttr("disabled");
            } else if (status=="Alerta" || bateria=="BateriaBaja"){
                $("#alarm-"+name+" .when").text(this.since);                
            } else if ( (status=="Reset") && isactive ) {
                activate_sound=false;
            }     
        });
    });
}

function zumbador() {
    if (activate_sound) {
        $.get( "/beep" );
    }
}

jQuery(".snooze").on("click",function() {
    activate_sound=false;
    $(this).attr("disabled","disabled");
});


$(".device").on("click", function(){
    var name = $(this).attr("data-name");
    console.log(name);
    jQuery.getJSON( "/devices/"+name, function( data ) {
        console.log(data);
        console.log(data.name);
        $("#name").val(data.name);
        $("#description").val(data.description);
        $("#tag").val(data.tag);
        $("#type").val(data.type);
        $("#dev-id").text(data.name);
    });
});

$(".btn-adddevice").on("click", function(){
    $("#editdevice").modal("show");
});

function getDevicesAndLog(){
    $.ajax({
        type:'GET',
        url:'/devices',
        dataType:'json',
        success:function(data){
            var s;
            $("#desktop_status").text("");
            $.each( data.desktop, function() {            
                $("#desktop_status").append("<li>[<b>"+this.name+"</b> "+this.tag+"]&nbsp;&nbsp;&nbsp;&nbsp;<i>"+this.description+"</i>&nbsp;&nbsp;<b>"+this.value+"</b>&nbsp;&nbsp;"+this.battery+"&nbsp;&nbsp;("+this.info+") </li>");
            });
        }
    });
    $.ajax({
        type:'GET',
        url:'/telegram_log',
        dataType:'json',
        success:function(data){
            $("#telegram_log").text(data.log);
        }
    });   
}

$("#btn-last-device").on("click", function(){
    getDevicesAndLog();
    $("#lastdevice").modal("show");
});

$("#refresh-last-device-info").on("click",getDevicesAndLog);

$("#getDeviceName").on("click", function(){
    var n;
    var nn;
    var d;
    
    console.log("clicking");
    
    $.ajax({
        type:'GET',
        url:'/last_telegram',
        dataType:'json',
        success:function(data){
            n=(data.id);
            nn=(data.id);
            console.log(n);
        }
    });
    
    function doPoll(){
        $.ajax({
            type:'GET',
            url:'/last_telegram',
            dataType:'json',
            success:function(data){
                nn=(data.id);
                console.log(nn);
                if(nn<n) {
                    d=data;
                    console.log(d.name);
                    $("#name").val(d.name);
                } else setTimeout(doPoll,1000);
            }
       }); 
    };
    
    doPoll();
});


function editDevice(e){
    e.preventDefault();
    console.log('submit');
     $("#getDeviceName").hide();
    
    var dev={
      name : $("#name").val(),
      description : $("#description").val(),
      tag : $("#tag").val(),
      type : $("#type").val()
    };
    console.log(dev);

     $.ajax({
        url: '/devices/'+dev.name, // url where to submit the request
        type : "POST", // type of action POST || GET
        dataType : 'json', // data type
        data : JSON.stringify(dev), // post data || get data
        success : function(result) {
            console.log(result);
            $("#editdevice").modal("hide");
            setTimeout(function(){ location.reload(true); },600);
        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    });

};


$("#openModalInternet").on("click", function(){
    $.getJSON('/configure',function(result) {
            console.log(result);
            $("#ssid").val(result.ssid);
            $("#password").val(result.password);
        }
    ); 
});


function editInternet(e){
    e.preventDefault();
    console.log('submit');
 
     $.ajax({
        url: '/configure',
        type : "POST", 
        data : $(this).serialize(),
        success : function(result) {
            console.log(result);
            $("#modal-internet").modal("hide");
            Swal.fire({
              title: 'Reinicio',
              text: "Para que los cambios surjan efecto, hay que reiniciar el sistema",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Reiniciar'
            }).then((result) => {
              if (result.value) {
                $.get( "/reset" );
              }
            })
        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    });

};


$("#borrardevice").on("click", function(e){
  e.preventDefault();
  console.log("borrardevice");
    
    var name = $("#name").val();

    Swal.fire({
      title: 'Borrar dispositivo',
      text: "¿Estas seguro?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si'
    }).then((result) => {

        if (result.value) {
            $.ajax({
                url: '/devices/'+name+'/delete', // url where to submit the request
                type : "POST", // type of action POST || GET
                success : function(result) {
                    console.log(result);
                    $("#editdevice").modal("hide");
                    setTimeout(function(){ location.reload(true); },600);
                },
                error: function(xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            });
      }
    })
  
});

function restart(){
    event.preventDefault();
    Swal.fire({
      title: 'Apagar',
      text: "¿Estas seguro?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        $.get( "/poweroff" );
      }
    })
}


$("#btn-poweroff").on("click", restart);


$("#internet-type").change( function(){

    var v = $(this).val();
    if(v=="wifi") $(".wifi").removeClass("d-none"); else $(".wifi").addClass("d-none");
    if( v!="no-connection") $(".info-ip").removeClass("d-none"); else $(".wifi").addClass("d-none");
});


/*$("#configuration-type").change( function(){
    var v = $(this).val();
    if(v=="manual") $(".manual").removeClass("d-none"); else $(".manual").addClass("d-none");
});*/


$( document ).ready(function() {
    
    $("form#editDevForm").submit(editDevice);

    $("form#editInternetForm").submit(editInternet);

    
    $('.btn-dismiss-alarm').on('click',function() {
        var name = $(this).attr("data-name");
       console.log(name);
       jQuery.getJSON( "/devices/"+name+"/reset", function( data ) {
            console.log(data);
       });
    })


    activate_sound=false;   
    setInterval(checkAlarm,5000);

    setInterval(zumbador,5000);
});
