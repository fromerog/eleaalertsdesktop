#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Librerias
import RPi.GPIO as GPIO
import keyboard
import time
from datetime import datetime
import sys
import traceback
import logging
import sqlite3 as lite

import ssl
import paho.mqtt.client as paho
import json
import re

try:
    import queue
except ImportError:
    import Queue as queue

# Constantes

FILE_DB = '/home/pi/elea/alertsdesktop/data.db'
CLIENT_ID = "ID"
MQTT_ADDRES = "alert.elea-soluciones.es"
BASE_TOPIC = "elea/alarm/"

TYPE_EVENT = "enocean"
TYPE_EVENT = "INTEGRATION"

alarm_topic_re = re.compile(r'^elea/alarm/(\d+)')

'''
    Event types
    1 Detector caida
    2 Aviso asistencia
    4 Reset
    6 Alarma actividad
    7 Bateria Baja
'''

#------------------------------------------------------------------------

def on_message(mosq, obj, msg):
    try:
        print("Recibido mensaje MQTT "+str(msg.topic+": " + str(msg.payload)))
        message = json.loads(msg.payload)

        m = alarm_topic_re.match(msg.topic)
        account_id = int(m.group(1))

        event_type = message['event_type']
        event_name = message['event_name']
        dev_info = message['dev_info']

        if event_type in [1,2,3,4,5,6,7,11]:
            
            info = event_name + " en " + dev_info
            value = "Alerta"
            if event_type==7: value = "BateriaBaja"
            if event_type==4: value = "Reset"

            print(info+" --> "+value)

            with lite.connect(FILE_DB) as con:
                cur=con.cursor()
                cur.execute("UPDATE gateways SET value=?, time=?, info=? WHERE name = ?",(value, datetime.now(), info, account_id))
                con.commit();
                cur.close();

    except ValueError:
        print("ValueError")
    except Exception as ex:
        print("Exception: "+str(ex))

#------------------------------------------------------------------------

def on_connect(client, userdata, flags, rc):
    print("Conectado: " + str(client) + " " + str(flags) + " " + str(rc))
    try:

        with lite.connect(FILE_DB) as con:
            cur=con.cursor()
            cur.execute("SELECT name FROM gateways")
            rows = cur.fetchall();

            topics = []
            for row in rows:
                topics.append( ( BASE_TOPIC+row[0],1) )

            rc = client.subscribe(topics)

            print("Client Subscribe Status: " + str(rc))
    except Exception as ex:
        print("Error en on_connect: " + str(ex))

#------------------------------------------------------------------------

def on_disconnect(client, userdata, rc):
    print("Desconectado: " + str(client) + " " + str(rc))
    while rc != 0:
        sleep(15)
        print("Reconectando...")
        try:
            rc = client.reconnect()
        except Exception as ex:
            print("Error reconectando MQTT en on_disconnect: " + str(ex))
            print("Reconectado con rc: " + str(rc))

#------------------------------------------------------------------------

def on_subscribe(client, userdata, mid, granted_qos):
    print("Suscrito: " + str(client) + " " + str(userdata) + " " + str(mid))

#------------------------------------------------------------------------

def on_unsubscribe():
    global mqtt
    print("Desuscrito: " + str(client) + " " + str(userdata) + " " + str(mid))
    try:
        mqtt.subscribe([(EVENT_TOPIC, 1)])
    except Exception as ex:
        print("Error en on_unsubscribe: " + str(ex))

#------------------------------------------------------------------------

def setup_MQTT():
    global mqtt

    while True:
        try:  # configuramos la conexion MQTT
            mqtt = paho.Client()
            mqtt.on_message = on_message
            mqtt.on_connect = on_connect
            mqtt.on_subscribe = on_subscribe
            mqtt.on_unsubscribe = on_unsubscribe
            mqtt.on_disconnect = on_disconnect
            #mqtt.tls_set(ca_certs="/etc/eleagw/EleaOpen/Configuracion/ca.crt",certfile="/etc/eleagw/EleaOpen/Configuracion/client.crt", keyfile="/etc/eleagw/EleaOpen/Configuracion/client.key", cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1, ciphers=None)
            #mqtt.tls_insecure_set(True)
            mqtt.connect(MQTT_ADDRES, 1883, 60)
            mqtt.loop_forever()
            break
        except Exception as ex:
            print("Error al conectar MQTT: " + str(ex))
            time.sleep(15)
            pass

#------------------------------------------------------------------------

def main():
    
    global EnoTelegrama
    
    print("\nListening to EleaAlerts Gateways - '^C' to exit\n==============================================")

    setup_MQTT()
    
if __name__ == '__main__':
    main()
