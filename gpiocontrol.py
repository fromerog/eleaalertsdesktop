import RPi.GPIO as GPIO
import time

BUZZER_GPIO = 18
#BUZZER_GPIO = 40  # para el caso de que usemos la placa enocean

class GPIOControl:
    def __init__(self):
        # la nomenclatura de pines de BOARD no cambia entre versiones, la de BCM si
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(BUZZER_GPIO, GPIO.OUT)  # led azul
        GPIO.output(BUZZER_GPIO, False)  # BLUE OFF

    def Beep(self,len):
        GPIO.output(BUZZER_GPIO, True)
        time.sleep(len)
        GPIO.output(BUZZER_GPIO, False)

    def Off(self):
        GPIO.output(BUZZER_GPIO, False)
        
try:
    class ScanDelegate(DefaultDelegate):
        def __init__(self):
            DefaultDelegate.__init__(self)
except Exception as ex:
    print ("Error creating ScanDelegate: " + str(ex))
