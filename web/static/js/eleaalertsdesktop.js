var activate_sound;

function addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
}

function checkAlarm(){
    
    //console.log(activate_sound);
    
    jQuery.getJSON( "/devices", function( data ) {
        var items = [];
        
        //console.log(data);
        
        $.each( data, function() {
            
            var isactive = false;
    
            if( $("#alarm-"+this.name).hasClass("bg-warning") || $("#alarm-"+this.name).hasClass("bg-danger") ) isactive=true; 
                        
            $("#list-"+this.name).removeClass("bg-warning bg-danger").addClass("bg-"+this.css);
            $("#alarm-"+this.name).removeClass("bg-warning bg-danger bg-none").addClass("bg-"+this.css);                   
           
           //if ( (this.status=="Alerta") && (!isactive) )  $.get( "/beep/1" );
            
            if ( (this.status=="Alerta") && (!isactive) ) {
                $("#alarm-"+this.name+" .when").text(this.last_time_seen);
                $.get( "/beep/3" );
                activate_sound=true;
                console.log("+");
                $(".snooze").removeAttr("disabled");                        
            } else if ( (this.status=="Reset") && isactive ) {
                activate_sound=false;
                console.log("-");
            }     
        });
    });
}

function zumbador() {
    if (activate_sound) {
        $.get( "/beep" );
    }
}

jQuery(".snooze").on("click",function() {
    activate_sound=false;
    //console.log("-");
    $(this).attr("disabled","disabled");
});
  
setInterval(function(){
    var currentdate = new Date(); 
    var datetime = addZero(currentdate.getDate()) + "/"
        + addZero((currentdate.getMonth()+1))  + "/" 
        + currentdate.getFullYear() + " @ "  
        + addZero(currentdate.getHours()) + ":"  
        + addZero(currentdate.getMinutes()) + ":" 
        + addZero(currentdate.getSeconds());
        jQuery(".time").text(datetime);
}, 1000);

$(".device").on("click", function(){
    var name = $(this).attr("data-name");
    console.log(name);
    jQuery.getJSON( "/devices/"+name, function( data ) {
        console.log(data);
        console.log(data.name);
        $("#name").val(data.name);
        $("#location").val(data.location);
        $("#tag").val(data.tag);
        $("#dev-id").text(data.id);
        $("#info").val(data.info);
        $("#rssi").text(data.RSSI);
    });
});

$(".btn-adddevice").on("click", function(){
    $("#editdevice").modal("show");
});

$(".btn-desktop").on("click", function(){
  var r = confirm("¿Estás seguro?");
  if (r == true) {
    window.location.href = "http://localhost/phpmyadmin/";
  }
});

$("#btn-last-device").on("click", function(){
    $.ajax({
        type:'GET',
        url:'/last_telegram',
        dataType:'json',
        success:function(data){
            $("#last-device-info span.device").text(data[0].device);
            $("#last-device-info span.RSSI").text(data[0].RSSI);
            $("#last-device-info span.RORG").text(data[0].RORG);
            $("#last-device-info span.timed").text(data[0].time);
            $("#last-device-info span.data").text(data[0].data);
        }
    });
    $("#lastdevice").modal("show");
});

$("#refresh-last-device-info").on("click", function(){
    $.ajax({
        type:'GET',
        url:'/last_telegram',
        dataType:'json',
        success:function(data){
            $("#last-device-info span.device").text(data[0].device);
            $("#last-device-info span.RSSI").text(data[0].RSSI);
            $("#last-device-info span.RORG").text(data[0].RORG);
            $("#last-device-info span.timed").text(data[0].time);
            $("#last-device-info span.data").text(data[0].data);
        }
    });
});

$("#getDeviceName").on("click", function(){
    var n;
    var nn;
    var d;
    
    console.log("clicking");
    
    $.ajax({
        type:'GET',
        url:'/last_telegram',
        dataType:'json',
        success:function(data){
            n=(data[0].id);
            nn=(data[0].id);
            console.log(n);
        }
    });
    
    function doPoll(){
        $.ajax({
            type:'GET',
            url:'/last_telegram',
            dataType:'json',
            success:function(data){
                nn=(data[0].id);
                console.log(nn);
                if(nn>n) {
                    d=data[0];
                    console.log(d.device);
                    $("#name").val(d.device);
                } else setTimeout(doPoll,1000);
            }
       }); 
    };
    
    doPoll();
});


function editDevice(e){
    e.preventDefault();
    console.log('submit');
     $("#getDeviceName").hide();
    
    var dev={
      name : $("#name").val(),
      location : $("#location").val(),
      tag : $("#tag").val(),
      info : $("#info").val(),
      id : $("#dev-id").text(),
      password: $("#password").val()
    };
    console.log(dev);

     $.ajax({
        url: '/devices/'+dev.name, // url where to submit the request
        type : "POST", // type of action POST || GET
        dataType : 'json', // data type
        data : JSON.stringify(dev), // post data || get data
        success : function(result) {
            // you can see the result from the console
            // tab of the developer tools
            console.log(result);
            $("#editdevice").modal("hide");
            //setTimeout(function(){ location.reload(true); },600);
        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    });

};


$("#borrardevice").on("click", function(e){
  e.preventDefault();
  console.log("borrardevice");

  var dev = {
    name : $("#name").val(),
    pass : $("#password").val()
  };

  $.ajax({
    url: '/devices/'+dev.name+'/delete', // url where to submit the request
    type : "POST", // type of action POST || GET
    dataType : 'json', // data type
    data : JSON.stringify(dev), // post data || get data
    success : function(result) {
        // you can see the result from the console
        // tab of the developer tools
        console.log(result);
        $("#editdevice").modal("hide");
        //setTimeout(function(){ location.reload(true); },600);
    },
    error: function(xhr, resp, text) {
        console.log(xhr, resp, text);
    }
}); 
  
});

$( document ).ready(function() {
    
    $("form#editDevForm").submit(editDevice);
    
    $('.btn-dismiss-alarm').on('click',function() {
        var name = $(this).attr("data-name");
       console.log(name);
       jQuery.getJSON( "/devices/"+name+"/reset", function( data ) {
            console.log(data);
       });
    })
    
    activate_sound=false;   
    setInterval(checkAlarm,5000);
    setInterval(zumbador,5000);
});