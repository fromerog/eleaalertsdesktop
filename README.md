# EleaAlertsDesktop

Centralita para Elea Alerts
---
Para acceder vía ssh:
user: pi
password:aele84824242
---
Una vez grabada la tarjeta con el FW, entrar en ella via ssh y actualizarla desde el repositorio con el comando
'git pull origin master' y ejecutar el script 'install-kiosko.sh'

Verificar que el archivo /home/pi/.config/lxsession/LXDE-pi/ contiene la linea

@chromium-browser --noerrdialogs --kiosk http://127.0.0.1:5000 --incognito --disable-translate --check-for-update-interval=31536000
