#!/usr/bin/python
# -*- coding: utf-8 -*-

# Librerias
import RPi.GPIO as GPIO
import sqlalchemy as db
from sqlalchemy.dialects.mysql import insert
import keyboard
import time
from datetime import datetime
from enocean.consolelogger import init_logging
import enocean.utils as utils
from enocean.communicators.serialcommunicator import SerialCommunicator
from enocean.protocol.packet import RadioPacket
from enocean.protocol.constants import PACKET, RORG
import sys
import traceback
import logging

try:
    import queue
except ImportError:
    import Queue as queue


logging.basicConfig(filename='/home/pi/eleaalertsdesktop/EleaAlertsDesktop.log', level=logging.DEBUG,
                    format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %H:%M:%S ')
logging.getLogger('enocean').propagate = False


beep = True     # emite sonido con cada telegrama recibido
#beep = False   # no emite sonido con cada telegrama recibido
sendReset = True # envia el reset al recibir una alarma
#sendReset = False # no envia el reset


# Configuracion del GPIO para habilitar el modulo EnOcean
GPIO.setmode(GPIO.BOARD)
GPIO.setup(33,GPIO.OUT)
GPIO.output(33,False)

GPIO.setmode(GPIO.BOARD)
GPIO.setup(18, GPIO.OUT)  # led azul

if beep:
    GPIO.output(18, True)
    time.sleep(0.2)
    GPIO.output(18, False)

                
# Tipos de datos

telegram_data = {
    'AF' : "Alerta",
    'F2' : "Reset",
    'F1' : "BateriaBaja",
    'F0' : "BateriaOk",
    'FC' : "Mantenimiento"
}

engine = None
connection = None
history = None
devices = None
device_list = []
device_index = {}
EnoTelegrama = None

# Cadena de conexi?n con BBDD
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://EleaAlertsDesktop:3l341234@localhost:3306/EADesktop'


def storeTelegram(tel): 
    global connection
    global devices
    global history
    global telegram_data
    global device_list
    global device_index
     
    print "->storeTelegram"
    
    if tel:
        if tel['device'] in device_index.keys():
            
            tel_id = device_index[tel['device']]
            
            print "\t-- " + tel['device']+ "("+str(tel['data'])+") found at "+ str(tel_id) +"!"
            event = telegram_data.setdefault(tel['data'], "Unknown")
            
            if event!="Unknown":
                if event=="BateriaBaja":
                    print "BateriaBaja"
                    query = devices.update().where(devices.c.id==tel_id).values( battery = "low", RSSI = tel['RSSI'])
            
                elif event=="BateriaOk":
                    print "BateriaOk"
                    query = devices.update().where(devices.c.id==tel_id).values( battery = "ok", RSSI = tel['RSSI'])
            
                else:
                    query = devices.update().where(devices.c.id==tel_id).values( status = event, RSSI = tel['RSSI'])
                
                print query
                connection.execute(query);
                
                if beep:
                    GPIO.output(18, True)
                    time.sleep(0.2)
                    GPIO.output(18, False)
        
        else:
            print '\t-- New Device Found ('+tel['device']+')'
        
        try:
            query = history.insert().values(RORG=tel['RORG'], data=tel['data'], device=tel['device'], RSSI=tel['RSSI'])
            print query
            print "insert"
            connection.execute(query)
        except:
            query = history.update().where(history.c.device==tel['device']).values(RORG=tel['RORG'], data=tel['data'], RSSI=tel['RSSI'])
            print query
            print "update"
            connection.execute(query)
        return None
        
    else:
        return None
    

def initBBDD():
        global engine
        global connection
        global history
        global devices
        global alarms
        global device_list
        global device_index
    
        engine = db.create_engine(SQLALCHEMY_DATABASE_URI, echo=False)
        print 'DB Tables : '
        print(engine.table_names())
        connection = engine.connect()
        metadata = db.MetaData()
        history = db.Table('history', metadata, autoload=True, autoload_with=engine)
        devices = db.Table('devices', metadata, autoload=True, autoload_with=engine)

        query = devices.select(devices.c.id)
        result = connection.execute(query)
        
        for row in result:
            device_list.append(row[1])
            device_index[row[1]] = row[0]
        
        print device_list
        print device_index

        return connection
    
    
def main():
    
    global EnoTelegrama
    
    print "\nInit Database\n============="

    try:
        connection = initBBDD()
    except:
        print "Problema conectando a la base de datos"

    print "\nInit Enocean\n============"
    
    init_logging()
    communicator = SerialCommunicator()
    communicator.start()
    time.sleep(1)
    EnoTelegrama = RadioPacket.create(RORG.RPS, 0x02, 0x06, None)
    EnoTelegramaVLD = RadioPacket.create(RORG.VLD, 0x01, 0x01, None)


    print "\nListening to EleaAlerts Network - '^C' to exit\n=============================================="

    exiting = False;

    while communicator.is_alive() and (exiting==False):
        try:
            packet = communicator.receive.get(block=True, timeout=1)
                  
            if (packet.packet_type == PACKET.RADIO_ERP1) and ((packet.rorg == RORG.RPS) or (packet.rorg == RORG.VLD)):
                
                ID = utils.to_hex_string(packet.data[2]) + utils.to_hex_string(packet.data[3]) + utils.to_hex_string(packet.data[4]) + utils.to_hex_string(packet.data[5])
                dato  = utils.to_hex_string(packet.data[1])
                status = utils.to_hex_string(packet.data[6])
                
                tel={
                    'RORG'  : 'RPS',
                    'data'  : dato,
                    'device': ID,
                    'RSSI'  : str(packet.dBm),
                    'status' : status
                }
                
                tel['RORG'] = 'VLD' if packet.rorg == RORG.VLD else 'RPS'
                print "["+datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "]  "+'-'.join(tel.values())
                                
                if ID in device_index.keys():
                
                    if (dato=="AF") and (packet.rorg == RORG.RPS) and sendReset:
                        EnoTelegrama.data[1] = 0xF3    
                        EnoTelegrama.data[6] = 0x20
                        communicator.send(EnoTelegrama)
                        print "\t\t-- " + "Sending F3 (RPS)"
                        #p = communicator.receive.get(block=True, timeout=0.01)   
                    elif (dato=="AF") and (packet.rorg == RORG.VLD) and sendReset:
                        EnoTelegramaVLD.data[1] = 0xF3
                        EnoTelegramaVLD.data[6] = 0x20
                        communicator.send(EnoTelegramaVLD)
                        print "\t\t-- " + "Sending F3 (VLD)"
                        #p = communicator.receive.get(block=True, timeout=0.01)
                
                storeTelegram(tel)

                    
        except IndexError:
            print "IndexError"
            continue

        except queue.Empty:
            continue

        except (KeyboardInterrupt, SystemExit):
            exiting = True
            break
    
        except Exception:
            traceback.print_exc(file=sys.stdout)
            exiting = True
            break    

    if communicator.is_alive():
       communicator.stop()

    
if __name__ == '__main__':
    main()
