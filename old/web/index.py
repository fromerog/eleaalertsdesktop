#!/usr/bin/python
# -*- coding: utf-8 -*-

# Librerias
import serial
import RPi.GPIO as GPIO
import sqlalchemy as db

import keyboard
import time
from datetime import datetime
import string, random

from flask import Flask
from flask import render_template
from flask import jsonify
from flask import request
from flask_sqlalchemy import SQLAlchemy

from collections import OrderedDict
import socket

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://EleaAlertsDesktop:3l341234@localhost:3306/EADesktop'
db = SQLAlchemy(app)

alarms_css = {
    "Alerta": "danger",
    "BateriaBaja": "warning",
    "Mantenimiento": "warning"
}

admin_password = "3l341234$"


try:
      s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      s.connect(("8.8.8.8", 80))
      localip=s.getsockname()[0]
      s.close()
except:
      localip="Sin red"

#############################################################

class GPIOControl:
    def __init__(self):
        # la nomenclatura de pines de BOARD no cambia entre versiones, la de BCM si
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(18, GPIO.OUT)  # led azul
        GPIO.output(18, False)  # BLUE OFF

    def Beep(self,len):
        GPIO.output(18, True)
        time.sleep(len)
        GPIO.output(18, False)

    def Off(self):
        GPIO.output(18, False)
        
try:
    class ScanDelegate(DefaultDelegate):
        def __init__(self):
            DefaultDelegate.__init__(self)
except Exception as ex:
    print ("Error creating ScanDelegate: " + str(ex))


EO_GPIO = GPIOControl()

############################################################

class History(db.Model):
    __tablename__ = 'history'
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    RORG = db.Column(db.String)
    device = db.Column(db.String)
    data = db.Column(db.String)
    RSSI = db.Column(db.Integer)
    time = db.Column(db.DateTime)
    timestamp = db.Column(db.TIMESTAMP)
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Device(db.Model):
    __tablename__ = 'devices'
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    name = db.Column(db.String)
    info = db.Column(db.String)
    location = db.Column(db.String)
    status = db.Column(db.String)
    created_in = db.Column(db.DateTime)
    last_time_seen = db.Column(db.DateTime)
    enabled = db.Column(db.Boolean)
    RSSI = db.Column(db.Integer)
    tag = db.Column(db.String)
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
    

class Alarm(db.Model):
    __tablename__ = 'alarms'
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    devide_id = db.Column(db.Integer, db.ForeignKey('devices.id', ondelete='SET NULL'))
    created = db.Column(db.DateTime)
    updated = db.Column(db.DateTime)
    type = db.Column(db.String)
    status = db.Column(db.String)
    info = db.Column(db.String)
    level = db.Column(db.Integer)
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}

############################################################

@app.route("/")
def home():
    global alarms_css
    
    devices = Device.query.all()
    out = {}
    for d in devices:
        dd=d.as_dict()
        dd["css"]=alarms_css.setdefault(d.status,'none')
        if d.tag not in out:
            out[d.tag] = [ dd ]
        else:
            out[d.tag].append(dd)
            
    outt = OrderedDict(sorted(out.items()))        
    
    return render_template("home.html",devices=outt,localip=localip);

############################################################

@app.route("/tags")
def tags():
    tags = []
    for tag in Device.query.distinct(Device.tag):
        if tag.tag not in tags: 
            tags.append(tag.tag)
    return jsonify(tags)

############################################################

@app.route("/devices")
def devices():
    devices = Device.query.all()
    out = []
    
    for d in devices:
        dd=d.as_dict()
        dd["css"]=alarms_css.setdefault(d.status,'none')       
        out.append(dd)
            
    return jsonify(out)

############################################################
############################################################

@app.route("/alarms")
def alarms():
    devices = Device.query.all()
    out = []
    for d in devices:
        dev=d.as_dict();
        if dev['status'] in ["Alerta","BateriaBaja","Mantenimiento"]:
            out.append(dev)
        
    return jsonify(out)

############################################################

@app.route("/devices/<string:name>/delete", methods=["POST"] )
def device_delete(name):
    
    req=request.get_json(force=True)
    if req['pass'] != admin_password :
        return
    try:
        dev = Device.query.filter(Device.name==name).first()   
        db.session.delete(dev)
    except Exception:
        pass
    return


@app.route("/devices/<string:name>", methods=["GET","POST"] )
def devicesbyname(name):
    
    dev = Device.query.filter(Device.name==name).first()   

    if dev is None:
        dev = Device()
        dev.name=''.join(random.choice(string.lowercase) for x in range(6))
        dev.location=""
        dev.tag=""
        dev.info=""
        dev.created_in=""
        dev.status=""
        dev.battery=""
        dev.RSSI=0
        dev.enabled=True
        db.session.add(dev)
   
    if request.method == 'POST':

        req=request.get_json(force=True)

        if req['password'] != admin_password :
            return
    
        dev.name=req['name']
        dev.location=req['location']
        dev.info=req['info']
        dev.tag=req['tag']
        db.session.commit()
    
    return jsonify(dev.as_dict())

############################################################

@app.route("/devices/<string:name>/reset")
def devicereset(name):
    
    dev = Device.query.filter(Device.name==name).first()   

    if dev is None:
        return
    
    dev.status="Reset"
    db.session.commit()
    
    return jsonify(dev.as_dict())

############################################################


@app.route("/last_telegram")
def last_telegram():

    full=[]
    try:
        dev = History.query.order_by(History.timestamp.desc()).first()
        d = dev.as_dict()
        d['id']=int(time.mktime(d['time'].timetuple()))
        full.append(d)

    except Exception:
        d = {"RORG":"RPS","RSSI":0,"data":"00","device":"repetir","id":0,}
        full.append(d) 
    return jsonify(full)

@app.route("/beep/<int:num>")
def beep(num):
    for i in range(num):
        EO_GPIO.Beep(0.5)
        time.sleep(0.5)
    EO_GPIO.Off()
    return jsonify( dict(status="ok" ) )


@app.route("/beep")
def beepone():
    EO_GPIO.Beep(1)
    EO_GPIO.Off()
    time.sleep(1)
    return jsonify( dict(status="ok" ) )

#############################################################
if __name__ == "__main__":
    app.run(debug=True, host='0,0,0,0')
    
